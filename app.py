# Serve model as a flask application
# https://towardsdatascience.com/simple-way-to-deploy-machine-learning-models-to-cloud-fd58b771fdcf
# https://stackoverflow.com/questions/51127344/tensor-is-not-an-element-of-this-graph-deploying-keras-model

import pickle
import numpy as np
from flask import Flask, request, render_template, redirect, url_for
from keras.models import load_model
from util import get_yolo_boxes
from bbox import draw_boxes
import cv2
import os
import json
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
import boto3

model = None
app = Flask(__name__)
bucket = None

def load_cnn_model():
    global model
    # model variable refers to the global variable
    model = load_model("yolov3_checkpoint_all.h5")
    os.environ['CUDA_VISIBLE_DEVICES']="0"
    global graph
    graph = tf.get_default_graph()

def load_s3_bucket(bucket_name):
    global bucket
    s3 = boto3.resource('s3', region_name='us-east-2')
    bucket = s3.Bucket(bucket_name)

@app.route('/')
def home_endpoint():
    return redirect(url_for('predict'))

@app.route('/result')
def result():
    print('results')
    return render_template("index.html")

@app.route('/predict', methods=['POST', 'GET'])
def predict():
    if request.method == 'POST':
        uploaded_file = request.files['image']
        image_path = uploaded_file.filename
        imgstr = uploaded_file.read()

        net_h, net_w = 320, 320 # a multiple of 32, the smaller the faster
        obj_thresh, nms_thresh = 0.5, 0.45
        try:
            npimg = np.fromstring(imgstr, np.uint8)
            image = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
            config_path = 'config.json'
            with open(config_path) as config_buffer:    
                config = json.load(config_buffer)
            output_path = "static/"
            if not os.path.isdir(output_path):
                os.makedirs(output_path)
            with graph.as_default():
                boxes = get_yolo_boxes(model, [image], net_h, net_w, config['model']['anchors'], obj_thresh, nms_thresh)[0]
            draw_boxes(image, boxes, config['model']['labels'], obj_thresh)
            image_name = image_path.split('/')[-1]
            full_filename = output_path + image_name
            image = np.uint8(image)
            cv2.imwrite(full_filename, image)
            return render_template("index.html", predicted_image=image_name)
        except:
            return "PREDICTION ERROR FOR IMAGE: " + image_path

    print('predict')
    return render_template("predict.html")


if __name__ == '__main__':
    load_cnn_model()  # load model at the beginning once only
    #load_s3_bucket('fotosdoartigo')
    app.run(host='0.0.0.0', port=80, debug=True)
