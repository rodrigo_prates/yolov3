import cv2
import numpy as np
import glob
from tqdm import tqdm
from colorama import Fore

img_array = []
for filename in tqdm(sorted(glob.glob('frames_input/*.jpg')), desc='video frames', 
        position=0, leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
    img = cv2.imread(filename)
    height, width, layers = img.shape
    size = (width,height)
    img_array.append(img)


out = cv2.VideoWriter('project.mp4',cv2.VideoWriter_fourcc(*'MP4V'), 15, size)
 
for i in range(len(img_array)):
    out.write(img_array[i])
out.release()