import os
import shutil


def organize_dataset(set_type, imagePath, annPath, outPath, input_type, ann_type):
    input_set = os.path.sep.join([outPath, set_type+'_input'])
    ann_set = os.path.sep.join([outPath, set_type+'_output'])

    if os.path.isdir(input_set) and os.listdir(input_set):
        shutil.rmtree(input_set)

    if not os.path.isdir(input_set):
        os.makedirs(input_set)

    if os.path.isdir(ann_set) and os.listdir(ann_set):
        shutil.rmtree(ann_set)

    if not os.path.isdir(ann_set):
        os.makedirs(ann_set)

    totalfiles = len(input_type)
    counter = 0
    for file in input_type:
        print("Copy input files process: {} %".format(round((100/totalfiles)*counter,2)))
        newfilepath = os.path.sep.join([input_set, file])
        origfilepath = os.path.sep.join([imagePath, file])
        shutil.copy2(origfilepath, newfilepath)
        counter += 1

    totalfiles = len(ann_type)
    counter = 0
    for file in ann_type:
        print("Copy input files process: {} %".format(round((100/totalfiles)*counter,2)))
        newfilepath = os.path.sep.join([ann_set, file])
        origfilepath = os.path.sep.join([annPath, file])
        shutil.copy2(origfilepath, newfilepath)
        counter += 1

def split_dataset(imagePath, annPath, outPath, train_set_file, val_set_file, 
                    test_set_file, img_limit=300):

    input_train = []
    ann_train = []

    input_val = []
    ann_val = []

    input_test = []
    ann_test = []

    train_files = open(train_set_file, 'r') 
    train_file_names = train_files.readlines()

    val_files = open(val_set_file, 'r') 
    val_file_names = val_files.readlines()

    test_files = open(test_set_file, 'r') 
    test_file_names = test_files.readlines() 

    count = 0
    for file in train_file_names:
        input_train.append(file.strip()+'.jpg')
        ann_train.append(file.strip()+'.xml')
        count += 1
        #if count >= img_limit:
        #    break

    count = 0
    for file in val_file_names:
        input_val.append(file.strip()+'.jpg')
        ann_val.append(file.strip()+'.xml')
        count += 1
        #if count >= img_limit:
        #    break

    count = 0
    for file in test_file_names:
        input_test.append(file.strip()+'.jpg')
        ann_test.append(file.strip()+'.xml')
        count += 1
        #if count >= img_limit:
        #    break

    organize_dataset('train', imagePath, annPath, outPath, input_train, ann_train)
    organize_dataset('valid', imagePath, annPath, outPath, input_val, ann_val)
    organize_dataset('test', imagePath, annPath, outPath, input_test, ann_test)


if __name__ == '__main__':

    imagePath = 'D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\JPEGImages'
    annPath = 'D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\Annotations'
    outPath = 'D:\\deep_learning_vending_machines\\vending_machines\\yolo_dataset'
    train_set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\train.txt'
    val_set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\val.txt'
    test_set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\test.txt'
    num_images = 300

    split_dataset(imagePath, annPath, outPath, train_set_file, val_set_file, test_set_file, img_limit=num_images)
