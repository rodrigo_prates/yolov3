import cv2
import os
import json
from keras.models import load_model

# extract frames from a video and save to directory as 'x.png' where 
# x is the frame index
video = 'project.mp4' # or 'rtsp://admin:123456@192.168.1.216/H264?ch=1&subtype=0'
config_path = 'config.json'
vidcap = cv2.VideoCapture(video)
count = 0
net_h, net_w = 416, 416 # a multiple of 32, the smaller the faster
obj_thresh, nms_thresh = 0.5, 0.45

with open(config_path) as config_buffer:    
    config = json.load(config_buffer)

os.environ['CUDA_VISIBLE_DEVICES'] = config['train']['gpus']
infer_model = load_model(config['train']['saved_weights_name'])

while vidcap.isOpened():
    success, image = vidcap.read()
    if success:
        #cv2.imwrite(os.path.join(path_output_dir, '%d.png') % count, image)

        count += 1
    else:
        break
cv2.destroyAllWindows()
vidcap.release()