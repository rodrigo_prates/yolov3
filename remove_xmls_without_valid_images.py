import os
import xml.etree.ElementTree as ET


xmls_path = 'D:\\deep_learning_vending_machines\\vending_machines\\yolo_dataset\\train_output'
images_path = 'D:\\deep_learning_vending_machines\\vending_machines\\yolo_dataset\\train_input'
count=0
for ann in sorted(os.listdir(xmls_path)):
    print('analisando arquivo: ', ann)
    xml_file = os.path.sep.join([xmls_path, ann])
    tree = ET.parse( xml_file )
    root = tree.getroot()
    imagename = root.find('filename').text
    imagepath = os.path.sep.join([images_path, imagename])
    if not os.path.isfile( imagepath ):
        count += 1
        os.remove(xml_file)
        print('imagem {} não encontrada. Removendo arquivo {}'.format(imagepath, xml_file))
print('Number of removed files: ', count)

