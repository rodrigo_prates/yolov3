import requests
import cv2
from skimage.metrics import structural_similarity as ssim
import os


url = "http://ec2-3-15-41-0.us-east-2.compute.amazonaws.comAhhh:80/predict"
images = [
    'D:/yolov3/imgs_14_06_2021/vlcsnap-00001.jpg',
    'D:/yolov3/imgs_14_06_2021/vlcsnap-00001.jpg',
    'D:/yolov3/imgs_14_06_2021/vlcsnap-00003.jpg',
    'D:/yolov3/imgs_14_06_2021/vlcsnap-00007.jpg'
]
payload={}
headers = {}

send_post=[False] * len(images)
for idx in range(0,len(images),2):
    image1 = cv2.imread(images[idx])
    image1g = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)
    image2 = cv2.imread(images[idx+1])
    image2g = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)
    s = ssim(image1g, image2g)
    if s < 1.0:
        send_post[idx] = True
        send_post[idx+1] = True

#if send_post:
files=[]
count=0
for idx in range(0, len(images)):
    if send_post[count]:

        files.append( 
            ("image"+str(idx+1), (os.path.basename(images[idx]), open(images[idx], 'rb'), 
                                                        'image/'+os.path.splitext(images[idx])[1][1:])) 
        )
    count+=1

response = requests.request("POST", url, headers=headers, files=files, data=payload)
print(response.text)