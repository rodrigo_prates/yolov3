FROM python:3.7-slim
COPY ./requirements.txt /deploy/
WORKDIR /deploy/
RUN apt update
RUN apt install build-essential -y
RUN gcc --version
RUN apt install -y libglib2.0-0 libsm6 libxrender1 libxext6
RUN pip install -r requirements.txt
EXPOSE 80