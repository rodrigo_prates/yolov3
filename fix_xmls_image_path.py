import os
import xml.etree.ElementTree as ET


xmls_path = 'D:\\deep_learning_vending_machines\\vending_machines\\yolo_dataset\\test_output'

for ann in sorted(os.listdir(xmls_path)):
    print('Corrigindo arquivo: ', ann)
    xml_file = os.path.sep.join([xmls_path, ann])
    tree = ET.parse( xml_file )
    root = tree.getroot()
    elem = root.find('filename')
    elem.text = os.path.basename(elem.text).split('.')[0] + '.jpg'
    if elem.text == '4588.jpg':
        print('achei no xml ', xml_file)
        break
    if elem.text == '6807.jpg':
        print('achei no xml ', xml_file)
        break
    tree.write(xml_file,encoding='UTF-8',xml_declaration=True)
