import requests
import cv2
from skimage.metrics import structural_similarity as ssim


images = [
    'D:/yolov3/imgs_14_06_2021/vlcsnap-00001.jpg',
    'D:/yolov3/imgs_14_06_2021/vlcsnap-00002.jpg',
    'D:/yolov3/imgs_14_06_2021/vlcsnap-00003.jpg',
    'D:/yolov3/imgs_14_06_2021/vlcsnap-00004.jpg'
]

send_post=False
for idx in range(0,len(images),2):
    image1 = cv2.imread(images[idx])
    image1g = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)
    image2 = cv2.imread(images[idx+1])
    image2g = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)
    s = ssim(image1g, image2g)
    if s < 1.0:
        send_post=True

if send_post:
    url = f'http://127.0.0.1:80/predict'
    files = {}
    for idx in range(0, len(images)):
        files["image"+str(idx+1)] = open(images[idx], 'rb').read()
    print(requests.post(url, files=files))
    #files = {
    #    'image1': open('D:/yolov3/imgs_14_06_2021/vlcsnap-00001.jpg', 'rb').read(), 
    #    'image2': open('D:/yolov3/imgs_14_06_2021/vlcsnap-00002.jpg', 'rb').read(),
    #    'image3': open('D:/yolov3/imgs_14_06_2021/vlcsnap-00003.jpg', 'rb').read(),
    #    'image4': open('D:/yolov3/imgs_14_06_2021/vlcsnap-00004.jpg', 'rb').read(),
    #    'image5': open('D:/yolov3/imgs_14_06_2021/vlcsnap-00005.jpg', 'rb').read(), 
    #    'image6': open('D:/yolov3/imgs_14_06_2021/vlcsnap-00006.jpg', 'rb').read(),
    #    'image7': open('D:/yolov3/imgs_14_06_2021/vlcsnap-00007.jpg', 'rb').read(),
    #    'image8': open('D:/yolov3/imgs_14_06_2021/vlcsnap-00008.jpg', 'rb').read()
    #}