import cv2
import numpy as np
from util import get_yolo_boxes
from bbox import draw_boxes

def predict_diff_images(imgstrs, graph, model, net_h, net_w, config, output_orig_path, output_pred_path, image_names):
    obj_thresh = config['model']['obj_thresh']
    nms_thresh = config['model']['nms_thresh']
    anchors = config['model']['anchors']
    labels = config['model']['labels']

    out = {}
    count=0
    for idx in range(0,len(imgstrs),2):
        image1 = cv2.imdecode(np.fromstring(imgstrs[idx], np.uint8), cv2.IMREAD_COLOR)
        image1_cp = image1.copy()

        image2 = cv2.imdecode(np.fromstring(imgstrs[idx+1], np.uint8), cv2.IMREAD_COLOR)
        image2_cp = image2.copy()

        with graph.as_default():
            boxes1 = get_yolo_boxes(model, [image1_cp], net_h, net_w, anchors, obj_thresh, nms_thresh)[0]
            boxes2 = get_yolo_boxes(model, [image2_cp], net_h, net_w, anchors, obj_thresh, nms_thresh)[0]

        draw_boxes(image1_cp, boxes1, config['model']['labels'], obj_thresh)
        draw_boxes(image2_cp, boxes2, config['model']['labels'], obj_thresh)

        full_filename = output_orig_path + '/' + image_names[idx]+".jpg"
        image1 = np.uint8(image1)
        cv2.imwrite(full_filename, image1)

        full_filename = output_pred_path + '/' + image_names[idx]+".jpg"
        image1_cp = np.uint8(image1_cp)
        cv2.imwrite(full_filename, image1_cp)

        full_filename = output_orig_path + '/' + image_names[idx+1]+".jpg"
        image2 = np.uint8(image2)
        cv2.imwrite(full_filename, image2)

        full_filename = output_pred_path + '/' + image_names[idx+1]+".jpg"
        image2_cp = np.uint8(image2_cp)
        cv2.imwrite(full_filename, image2_cp)

        labelsI = []
        labelsI_str = []
        dict1 = dict.fromkeys(labels, 0)
        for box in boxes1:
            label = -1
            label_str = ''
            for i in range(len(labels)):
                if box.classes[i] > obj_thresh:
                    if label_str != '': label_str += ', '
                    label_str += (labels[i] + ' ' + str(round(box.get_score()*100, 2)) + '%')
                    label = i
                    labelsI.append(label)
                    labelsI_str.append(label_str)
                    dict1[labels[i]] += 1

        labelsI = []
        labelsI_str = []
        dict2 = dict.fromkeys(labels, 0)
        for box in boxes2:
            label = -1
            label_str = ''
            for i in range(len(labels)):
                if box.classes[i] > obj_thresh:
                    if label_str != '': label_str += ', '
                    label_str += (labels[i] + ' ' + str(round(box.get_score()*100, 2)) + '%')
                    label = i
                    labelsI.append(label)
                    labelsI_str.append(label_str)
                    dict2[labels[i]] += 1

        dict_diff1 = dict.fromkeys(labels, 0)
        for k in dict_diff1:
            dict_diff1[k] = dict2[k] - dict1[k]

        count +=1
        out['Image'+str(idx)] = dict1
        out['Image'+str(idx+1)] = dict2
        out['Images_Diff'+str(count)] = dict_diff1

    return out


# def predict_diff_images(imgstr1, imgstr2, imgstr3, imgstr4, graph, model, net_h, net_w, config, 
#                 output_orig_path, output_pred_path, image_name1, image_name2, image_name3, image_name4):

#     image1 = cv2.imdecode(np.fromstring(imgstr1, np.uint8), cv2.IMREAD_COLOR)
#     image1_cp = image1.copy()
                
#     image2 = cv2.imdecode(np.fromstring(imgstr2, np.uint8), cv2.IMREAD_COLOR)
#     image2_cp = image2.copy()

#     image3 = cv2.imdecode(np.fromstring(imgstr3, np.uint8), cv2.IMREAD_COLOR)
#     image3_cp = image3.copy()
                
#     image4 = cv2.imdecode(np.fromstring(imgstr4, np.uint8), cv2.IMREAD_COLOR)
#     image4_cp = image4.copy()

#     obj_thresh = config['model']['obj_thresh']
#     nms_thresh = config['model']['nms_thresh']
#     anchors = config['model']['anchors']

#     with graph.as_default():
#         boxes1 = get_yolo_boxes(model, [image1_cp], net_h, net_w, anchors, obj_thresh, nms_thresh)[0]
#         boxes2 = get_yolo_boxes(model, [image2_cp], net_h, net_w, anchors, obj_thresh, nms_thresh)[0]
#         boxes3 = get_yolo_boxes(model, [image3_cp], net_h, net_w, anchors, obj_thresh, nms_thresh)[0]
#         boxes4 = get_yolo_boxes(model, [image4_cp], net_h, net_w, anchors, obj_thresh, nms_thresh)[0]
#     draw_boxes(image1_cp, boxes1, config['model']['labels'], obj_thresh)
#     draw_boxes(image2_cp, boxes2, config['model']['labels'], obj_thresh)
#     draw_boxes(image3_cp, boxes3, config['model']['labels'], obj_thresh)
#     draw_boxes(image4_cp, boxes4, config['model']['labels'], obj_thresh)

#     full_filename = output_orig_path + '/' + image_name1
#     image1 = np.uint8(image1)
#     cv2.imwrite(full_filename, image1)

#     full_filename = output_pred_path + '/' + image_name1
#     image1_cp = np.uint8(image1_cp)
#     cv2.imwrite(full_filename, image1_cp)
            
#     full_filename = output_orig_path + '/' + image_name2
#     image2 = np.uint8(image2)
#     cv2.imwrite(full_filename, image2)

#     full_filename = output_pred_path + '/' + image_name2
#     image2_cp = np.uint8(image2_cp)
#     cv2.imwrite(full_filename, image2_cp)

#     full_filename = output_orig_path + '/' + image_name3
#     image3 = np.uint8(image3)
#     cv2.imwrite(full_filename, image3)

#     full_filename = output_pred_path + '/' + image_name3
#     image3_cp = np.uint8(image3_cp)
#     cv2.imwrite(full_filename, image3_cp)

#     full_filename = output_orig_path + '/' + image_name4
#     image4 = np.uint8(image4)
#     cv2.imwrite(full_filename, image4)

#     full_filename = output_pred_path + '/' + image_name4
#     image4_cp = np.uint8(image4_cp)
#     cv2.imwrite(full_filename, image4_cp)

#     labels = config['model']['labels']

#     labelsI = []
#     labelsI_str = []
#     dict1 = dict.fromkeys(labels, 0)
#     for box in boxes1:
#         label = -1
#         label_str = ''
#         for i in range(len(labels)):
#             if box.classes[i] > obj_thresh:
#                 if label_str != '': label_str += ', '
#                 label_str += (labels[i] + ' ' + str(round(box.get_score()*100, 2)) + '%')
#                 label = i
#                 labelsI.append(label)
#                 labelsI_str.append(label_str)
#                 dict1[labels[i]] += 1

#     labelsI = []
#     labelsI_str = []
#     dict2 = dict.fromkeys(labels, 0)
#     for box in boxes2:
#         label = -1
#         label_str = ''
#         for i in range(len(labels)):
#             if box.classes[i] > obj_thresh:
#                 if label_str != '': label_str += ', '
#                 label_str += (labels[i] + ' ' + str(round(box.get_score()*100, 2)) + '%')
#                 label = i
#                 labelsI.append(label)
#                 labelsI_str.append(label_str)
#                 dict2[labels[i]] += 1

#     labelsI = []
#     labelsI_str = []
#     dict3 = dict.fromkeys(labels, 0)
#     for box in boxes3:
#         label = -1
#         label_str = ''
#         for i in range(len(labels)):
#             if box.classes[i] > obj_thresh:
#                 if label_str != '': label_str += ', '
#                 label_str += (labels[i] + ' ' + str(round(box.get_score()*100, 2)) + '%')
#                 label = i
#                 labelsI.append(label)
#                 labelsI_str.append(label_str)
#                 dict3[labels[i]] += 1

#     labelsI = []
#     labelsI_str = []
#     dict4 = dict.fromkeys(labels, 0)
#     for box in boxes4:
#         label = -1
#         label_str = ''
#         for i in range(len(labels)):
#             if box.classes[i] > obj_thresh:
#                 if label_str != '': label_str += ', '
#                 label_str += (labels[i] + ' ' + str(round(box.get_score()*100, 2)) + '%')
#                 label = i
#                 labelsI.append(label)
#                 labelsI_str.append(label_str)
#                 dict4[labels[i]] += 1

#     dict_diff1 = dict.fromkeys(labels, 0)
#     for k in dict_diff1:
#         dict_diff1[k] = dict2[k] - dict1[k]

#     dict_diff2 = dict.fromkeys(labels, 0)
#     for k in dict_diff2:
#         dict_diff2[k] = dict4[k] - dict3[k]

#     return {'Image1': dict1, 'Image2': dict2, 'Images_Diff1': dict_diff1, 
#                 'Image3': dict3, 'Image4': dict4, 'Images_Diff2': dict_diff2}