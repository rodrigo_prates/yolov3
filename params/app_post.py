# Serve model as a flask application
# https://towardsdatascience.com/simple-way-to-deploy-machine-learning-models-to-cloud-fd58b771fdcf
# https://stackoverflow.com/questions/51127344/tensor-is-not-an-element-of-this-graph-deploying-keras-model

import os
import json

config_path = '/deploy/params/config.json'
with open(config_path) as config_buffer:    
    config = json.load(config_buffer)
os.environ['CUDA_VISIBLE_DEVICES']=config['train']['gpus']

from flask import Flask, request
from keras.models import load_model
import tensorflow as tf
from datetime import datetime
from predict_diff_images import predict_diff_images


model = None
app = Flask(__name__)
bucket = None

def load_cnn_model():
    global model
    # model variable refers to the global variable
    model = load_model("/deploy/params/checkpoint.h5")
    global graph
    graph = tf.get_default_graph() 

@app.route('/')
def home_endpoint():
    return 'Call from prompt: curl -X POST 127.0.0.1:80/predict -H "Content-Type: multipart/form-data" -F "image=@test_input/1006.jpg"' \
        'or -F "image1=@real_valid_input/opencv_frame_23.png" -F "image2=@real_valid_input/opencv_frame_23.png"'

#curl -X POST 127.0.0.1:80/predict -H "Content-Type: application/json" -d "test_input\1000.jpg"
#curl -X POST 127.0.0.1:80/predict -H "Content-Type: multipart/form-data" -F "image=@test_input/1006.jpg"
#curl -X POST ec2-18-191-219-197.us-east-2.compute.amazonaws.com:80/predict -H "Content-Type: application/json" -d "test_input\1000.jpg"
@app.route('/predict', methods=['POST', 'GET'])
def predict():
    if request.method == 'POST':

        num_images = len(request.files)
        image_paths = []
        image_names = []
        imgstrs = []
        for idx in range(0, num_images):
            k = list(request.files.keys())[idx]
            image_path = request.files[k].filename
            image_paths.append(image_path)
            image_name = image_path.split('/')[-1]
            image_names.append(image_name)
            imgstr = request.files[k].read()
            imgstrs.append(imgstr)
        
        net_h, net_w = 320, 320 # a multiple of 32, the smaller the faster
        #obj_thresh, nms_thresh = 0.5, 0.35
        today = datetime.now()
        output_pred_path = "/deploy/params/output/predictions_"+today.strftime('%Y%m%d')
        output_orig_path = "/deploy/params/output/originals_"+today.strftime('%Y%m%d')
        if not os.path.isdir(output_pred_path):
            os.makedirs(output_pred_path)
        if not os.path.isdir(output_orig_path):
            os.makedirs(output_orig_path)
        try:
            out = predict_diff_images(imgstrs, graph, model, net_h, net_w, config, output_orig_path, output_pred_path, image_names)
            f = open('/deploy/params/output.txt', "a")
            f.write("{\n")
            for k in out.keys():
                f.write("'{}':'{}'\n".format(k, out[k]))
            f.write("}")
            f.close()
            print(out)           
            return out
        except:
            return "PREDICTION ERROR FOR IMAGES: " + str(image_names)


if __name__ == '__main__':
    load_cnn_model()  # load model at the beginning once only
    app.run(host='0.0.0.0', port=80, debug=True)
