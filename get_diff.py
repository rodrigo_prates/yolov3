import os
import argparse
import json
import cv2
import numpy as np
from keras.models import load_model
from utils.utils import get_yolo_boxes


def _main_(args):
    config_path  = args.conf
    input_path1   = args.input1
    input_path2   = args.input2

    with open(config_path) as config_buffer:    
        config = json.load(config_buffer)

    ###############################
    #   Set some parameter
    ###############################       
    net_h, net_w = 416, 416 # a multiple of 32, the smaller the faster
    obj_thresh, nms_thresh = 0.5, 0.45

    ###############################
    #   Load the model
    ###############################
    os.environ['CUDA_VISIBLE_DEVICES'] = config['train']['gpus']
    infer_model = load_model(config['train']['saved_weights_name'])

    # do images detections

    image1 = cv2.imread(input_path1)
    image2 = cv2.imread(input_path2)

    boxes1 = get_yolo_boxes(infer_model, [image1], net_h, net_w, config['model']['anchors'], obj_thresh, nms_thresh)[0]

    boxes2 = get_yolo_boxes(infer_model, [image2], net_h, net_w, config['model']['anchors'], obj_thresh, nms_thresh)[0]

    labels = config['model']['labels']

    labels1 = []
    labels_str1 = []
    dict1 = dict.fromkeys(labels, 0)
    for box in boxes1:
        label = -1
        label_str = ''
        for i in range(len(labels)):
            if box.classes[i] > obj_thresh:
                if label_str != '': label_str += ', '
                label_str += (labels[i] + ' ' + str(round(box.get_score()*100, 2)) + '%')
                label = i
                labels1.append(label)
                labels_str1.append(label_str)
                dict1[labels[i]] += 1
        #print(label_str)
    print("Resultado imagem: ", input_path1)
    print(dict1)

    labels2 = []
    labels_str2 = []
    dict2 = dict.fromkeys(labels, 0)
    for box in boxes2:
        label = -1
        label_str = ''
        for i in range(len(labels)):
            if box.classes[i] > obj_thresh:
                if label_str != '': label_str += ', '
                label_str += (labels[i] + ' ' + str(round(box.get_score()*100, 2)) + '%')
                label = i
                labels2.append(label)
                labels_str2.append(label_str)
                dict2[labels[i]] += 1
        #print(label_str)
    print("Resultado imagem: ", input_path2)
    print(dict2)

    print("Diferença:")
    dict_diff = dict.fromkeys(labels, 0)
    for k in dict_diff:
        dict_diff[k] = dict2[k] - dict1[k]
    print(dict_diff)



if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='prediction of purchased drink')
    argparser.add_argument('-c', '--conf', help='path to configuration file')
    argparser.add_argument('-ib', '--input1', help='complete path to image before')
    argparser.add_argument('-ia', '--input2', help='complete path to image after')

    args = argparser.parse_args()
    _main_(args)
